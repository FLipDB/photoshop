#include <stdio.h>
// Use exit
#include <stdlib.h>
// Including all headers files
#include "./scripts/Requirements.h"
#include "./scripts/Installer.h"
#include "./scripts/Cameraraw.h"
#include "./scripts/Configure.h"
#include "./scripts/Uninstaller.h"

/*
Thanks especially to Gictorbit, because his script has served me as a reference and 
I have added some things: https://github.com/Gictorbit/photoshopCClinux

Also thanks to C-Y-P-H-E-R for US translations (now not needed because
I include in this repo the native version of US Photoshop.)

Also thanks to the people at WineHQ, especially the maintainers Bartosz Kosiorek 
and Baalaji Balasubramani for publishing 
their tests in v21: https://appdb.winehq.org/objectManager.php?sClass=version&iId=38516
As they have done with v22 of Photoshop: https://appdb.winehq.org/objectManager.php?sClass=version&iId=39717

Unfortunately v22 has not worked for me, although it may be because my PC does not 
meet any requirement (since they have been increased).

The current version of this Photoshop is:
*/
const char CURRENT_PH_VERSION[] = "21.2.4";

int main()
{
    int option = 0;
    while (1)
    {
        system("clear");
        printf("Welcome to Photoshop CC v%s Linux Installer\n"
                "Choose one of the following options:\n"
                "1. Check if your computer meets the requirements to run Photoshop\n"
                "2. Install Photoshop\n"
                "3. Install Adobe Camera Raw v12\n"
                "4. Configure Wine\n"
                "5. Uninstall Photoshop\n"
                "6. Exit the program\n", CURRENT_PH_VERSION);
        scanf("%u", &option);
        switch (option)
        {
            case 1:
                check_requirements();
                break;
            case 2:
                install_photoshop();
                break;
            case 3:
                install_cameraraw();
                break;
            case 4:
                configure_wine();
                break;
            case 5:
                uninstall_photoshop();
                break;
            default:
                return 0;
        }
    }
}

int return_program()
{
    getchar();
    // Return
    char answer;
    printf("\nDo you want to go back to the main menu?[n,y] ");
    scanf("%c", &answer);
    if (answer != 'y')
    {
        exit(0);
    }
    return 0;
}

int check_memory_allocation(void *ptr)
{
    // Check if the allocation of the given pointer fails or not.
    if (ptr == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory allocation.");
        exit(1);
    }
    return 0;
}
