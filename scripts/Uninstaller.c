// The objective of this file is to install Adobe Camera Raw v12
#include "Photoshop.h"
#include "Uninstaller.h"

#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

void complete_uninstall(char *location);
void partial_uninstall(char *location);

int uninstall_photoshop()
{
    printf("Welcome to the uninstaller of Photoshop v21\n");
    printf("Please, remember me some questions for a more personalized uninstall:\n");
    // Ask if he made a installation in all system wide or only in his user
    char *answer = malloc(sizeof(char) * 5);
    if (answer == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
        exit(1);
    }
    printf("You have installed Photoshop in the whole system or only in your user??[whole, only]: ");
    getchar();
    scanf("%s", answer);
    // Delete icon
    system("sudo rm -vf /usr/share/icons/hicolor/256x256/apps/photoshop-cc.png");
    // Deleting .desktop
    if (answer[0] == 'o')
    {
        system("rm -vf $HOME/.local/share/applications/photoshop.desktop");
    }
    else if (answer[0] == 'w')
    {
        system("sudo rm -vf /usr/share/applications/photoshop.desktop");
    }
    free(answer);
    // Ask for installation location
    char *location = malloc(sizeof(char) * 150);
    check_memory_allocation(location);
    printf("What is the installation path that you have written in the installer? (Absolute Path):\n");
    getchar();
    scanf("%s", location);
    // Finally ask if want uninstall all, including Wine and winetricks
    answer = malloc(sizeof(char) * 5);
    check_memory_allocation(answer);
    printf("You want uninstall all (Wine, winetricks and Photoshop files)\n or only all Photoshop files (included Camera Raw)?[all, only]: ");
    getchar();
    scanf("%s", answer);
    if (tolower(answer[0]) == 'a')
    {
        printf("Starting complete uninstaller...");
        complete_uninstall(location);
    }
    else
    {
        printf("Starting partial uninstaller...");
        partial_uninstall(location);
    }
    free(answer);
    free(location);
    return_program();
}

void complete_uninstall(char *location)
{
    // First check what distribution is
    char *distribution = malloc(sizeof(int));
    check_memory_allocation(distribution);
    printf("Choose one of the following supported distros: \n"
            "1. Debian or any other based distro except Ubuntu \n"
            "2. Ubuntu or a Ubuntu based distro \n"
            "3. Arch Linux or any other based distro \n"
            "4. Fedora \n"
            "5. Gentoo Linux \n"
            "Write here: ");
    scanf("%u", distribution);
    switch (*distribution)
    {
        case 1 ... 2:
            // Uninstalling packages is the same in Debian that in Ubuntu
            system("sudo apt-get remove wine-stable wine-stable-amd64 ");
            break;
        case 3:
            printf("Uninstalling WineHQ Stable...\n");
            system("sudo pacman -Rsn wine");
            break;
        case 4:
            system("sudo dnf remove winehq-stable");
            break;
        case 5:
            system("emerge -Cv app-emulation/wine-vanilla net-misc/wget");
            break;
        default:
            printf("Actually the distro is not supported.");
            free(distribution);
            return 1;
    }
    // Free memory of distribution
    free(distribution);
    // Uninstall winetricks
    system("sudo rm -r /usr/local/bin/winetricks");
    // Now remove all files of Photoshop
    partial_uninstall(location);
};

void partial_uninstall(char *location)
{
    // Delete only the Photoshop folder
    char *command = malloc(sizeof(char) * 250);
    check_memory_allocation(command);
    sprintf(command, "sudo rm -rvf %s/PhotoshopCC", location);
    system(command);
};