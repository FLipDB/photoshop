// The objective of this file it is to configure a prefix created
#include "Photoshop.h"
#include "Configure.h"
#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>

int configure_wine()
{
    char *location = malloc(sizeof(char) * 200);
    check_memory_allocation(location);
    printf("What is the installation path that you have written in the installer? (Absolute Path):\n");
    scanf("%s", location);
    int max_len = sizeof(char) * 250;
    char *wine_command = malloc(max_len);
    check_memory_allocation(wine_command);
    snprintf(wine_command, max_len, "WINEPREFIX=%s/.prefix winecfg", location);
    printf("%s", wine_command);
    system(wine_command);
    // Free memory of wine command and location
    free(wine_command);
    free(location);
    return_program();
}