/* 
The objective of this file is install all the necessary stable 
dependencies and configure it based on linux distribution.
*/
#include "Photoshop.h"
#include "Installer.h"
#include <stdio.h>
// Use malloc and exit
#include <stdlib.h>
// Use this for make syscalls
#include <sys/sysinfo.h>
// Use this for atoi, conversor string to int, strcmp...
#include <string.h>
#include <stdbool.h> // Header-file for boolean data-type.
#include <ctype.h> // Char tools
#include <unistd.h> // sleep function

void setup_fedora();
void setup_debian();
void set_dark_mode(char *location);
void change_desktop(char *location);

int install_photoshop()
{
    // Inform to user that the installation is started
    system("notify-send 'Photoshop CC' 'Starting Installer of Photoshop' --icon=$PWD/images/photoshop-cc.png");
    // Depending on distribution, make the installation with their
    // corresponding managment package
    char *distribution = malloc(sizeof(int));
    printf("Choose one of the following supported distros: \n"
            "1. Debian or any other based distro except Ubuntu \n"
            "2. Ubuntu or a Ubuntu based distro \n"
            "3. Arch Linux or any other based distro \n"
            "4. Fedora \n"
            "5. Gentoo Linux \n"
            "Write here: ");
    scanf("%u", distribution);
    switch (*distribution)
    {
        case 1:
            setup_debian();
            break;
        case 2:
            system("sudo apt install wine64 wget cabextract");
            break;
        case 3:
            printf("Installing WineHQ Stable...\n");
            system("sudo pacman -S wine wget");
            break;
        case 4:
            setup_fedora();
            break;
        case 5:
            system("emerge -av app-emulation/wine-vanilla net-misc/wget");
            break;
        default:
            printf("Actually the distro is not supported.");
            free(distribution);
            return 1;
    }
    // Free memory of the answer of distribution
    free(distribution);
    /* 
    Once specific installation is done, now configure all
    necessary tools for run Photoshop and download it.
    First install winetricks
    */
    printf("\nInstalling winetricks...\n");
    sleep(1);
    system("wget https://raw.githubusercontent.com/Winetricks/winetricks/master/src/winetricks; chmod +x winetricks; sudo mv winetricks /usr/local/bin/");
    // Ask for the installation location
    printf("\nWhere do you want to install Photoshop (Absolute Route)?:\n");
    printf("Example of route (realize that in the end it doesn't end with /): /home/your_username\n");
    char *location = malloc(sizeof(char) * 150);
    check_memory_allocation(location);
    scanf("%s", location);
    // Create directory structure, set wine prefix, install winetricks and configure it with winecfg.
    printf("Installing some libraries, now it install automatically without confirmation.\n");
    sleep(2);
    // Declaring max_len for save the total of bytes of x thing and use it on snprintf (more safe)
    int max_len = sizeof(char) * 350;
    char *prefix = malloc(max_len);
    check_memory_allocation(prefix);
    // Is important the argument -f for force, beacuse if checksum sha256 fail, the program can't continue
    snprintf(prefix, max_len * 350,
            "mkdir %1$s/PhotoshopCC; WINEPREFIX=%1$s/PhotoshopCC winetricks win10 --force --unattended --verbose vcrun2019 vcrun2012 vcrun2013 vcrun2010 fontsmooth=rgb gdiplus msxml3 msxml6 atmlib corefonts dxvk", 
            location);
    system(prefix);
    free(prefix);
    // Open winecfg for config the prefix
    max_len = sizeof(char) * 250;
    prefix = malloc(max_len);
    check_memory_allocation(prefix);
    snprintf(prefix, max_len, "WINEPREFIX=%s/PhotoshopCC winecfg", location);
    system(prefix);
    free(prefix);
    // Set dark mode of wine
    char *answer = malloc(sizeof(char) * 8);
    check_memory_allocation(answer);
    printf("\nDo you want to set the night mode? [yes/no]: ");
    scanf("%s", answer);
    if (tolower(answer[0]) == 'y')
    {
        set_dark_mode(location);
    }
    // Download Photoshop, ask for the prefered language and move inside .prefix
    max_len = sizeof(char) * 350;
    char *installation_location = malloc(max_len);
    check_memory_allocation(installation_location);
    // Choose the language of Photoshop
    printf("Do you want the English (is the international variant) or Spanish language. Write one of the following: [english/spanish] ?: ");
    scanf("%s", answer);
    char language[] = "PhotoshopCCSpanish";
    if (tolower(answer[0]) == 'e')
    {
        strcpy(language, "PhotoshopCCEnglish");
    }
    snprintf(installation_location, max_len, "tar -xvf ./files/%s.tar.xz -C %s/PhotoshopCC", language, location);
    printf("Copying Photoshop files...\n");
    sleep(1);
    system(installation_location);
    free(installation_location);
    // Copy icon and .desktop for appear in gnome apps
    system("sudo cp ./images/photoshop-cc.png /usr/share/icons/hicolor/256x256/apps");
    // Finally ask if want install Photoshop in all system or only in his current user
    
    printf("Do you want to install Photoshop in your user or in all[only/all] ?: ");
    scanf("%s", answer);
    change_desktop(location);
    printf("Creating a shortcut for launch Photoshop \n");
    if (answer[0] == 'o')
    {
        system("mv -v -f ./files/photoshop.desktop $HOME/.local/share/applications");
    }
    else
    {
        system("sudo mv -v -f ./files/photoshop.desktop /usr/share/applications");
    };
    // Free memory of location
    free(location);
    free(answer);
    // Inform the user that everything is complete
    system("notify-send 'Photoshop CC' 'Finishing the installation...' --icon=/usr/share/icons/hicolor/256x256/apps/photoshop-cc.png");
    printf("Installation completed successfully! Opening Photoshop CC v%s\n", CURRENT_PH_VERSION);
    system("nohup gtk-launch photoshop </dev/null >/dev/null 2>&1 &");
    return_program();
}

void setup_fedora()
{
    // First update repos and packages
    system("sudo dnf upgrade");
    // Next install required packages, for this I want use the latest stable
    // version from the official repo
    system("sudo dnf -y install dnf-plugins-core");
    // Adding official repo for download WineHQ depending on the version of Fedora
    printf("What version of Fedora do you have?: ");
    int *version = malloc(sizeof(int));
    if (version == NULL)
    {
        fprintf(stderr, "The program has crashed due to memory.");
    }
    scanf("%i", version);
    int max_len = sizeof(char) * 100;
    char *command = malloc(max_len);
    check_memory_allocation(command);
    snprintf(command, max_len, sizeof(char) * 100, "sudo dnf config-manager --add-repo https://dl.winehq.org/wine-builds/fedora/%i/winehq.repo", *version);
    system(command);
    // Free memory of version and command
    free(version);
    free(command);
    // Installing WineHQ stable and winetricks
    printf("Installing WineHQ Stable...");
    // Using the argument --allowerasing because by default Fedora has a package of Wine installed
    system("sudo dnf -y install --allowerasing winehq-stable wget");
    /*
    One very important thing, add the Wine Stable installation to PATH, because by default it is not added.
    If you have a different shell than the default one on your system, the following command will only work 
    until you close the terminal. Then it will not detect the installation of Wine,
    although photoshop will open as a normal app.
    */
    system("export PATH=\"/opt/wine-stable/bin:${PATH}\"");
}

void setup_debian()
{
    system("sudo apt-get update && apt-get upgrade");
    // Enabling 32 bit architecture
    system("sudo dpkg --add-architecture i386");
    // Add WineHQ repository
    system("sudo apt install gnupg2 software-properties-common wget");
    system("wget -qO - https://dl.winehq.org/wine-builds/winehq.key | sudo apt-key add -");
    system("sudo apt-add-repository https://dl.winehq.org/wine-builds/debian/");
    // Update APT
    system("sudo apt update");
    system("wget -O- -q https://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10/Release.key | sudo apt-key add -");
    system("echo 'deb http://download.opensuse.org/repositories/Emulators:/Wine:/Debian/Debian_10 ./' | sudo tee /etc/apt/sources.list.d/wine-obs.list");
    // Finally install WineHQ from stable branch
    system("sudo apt update");
    printf("Installing WineHQ Stable...");
    system("sudo apt install --install-recommends winehq-stable");
    /*
    One very important thing, add the Wine Stable installation to PATH, because by default it is not added.
    If you have a different shell than the default one on your system, the following command will only work 
    until you close the terminal. Then it will not detect the installation of Wine,
    although photoshop will open as a normal app.
    */
    system("export PATH=\"/opt/wine-stable/bin:${PATH}\"");
}

void set_dark_mode(char *location)
{
    // Create string type for manage array of dark color, use \ for escape characters
    char colors[31][38] = 
    {
        {"[Control Panel\\Colors] 1491939580"},
        {"#time=1d2b2fb5c69191c"},
        {"\"ActiveBorder\"=\"49 54 58\""},
        {"\"ActiveTitle\"=\"49 54 58\""},
        {"\"AppWorkSpace\"=\"60 64 72\""},
        {"\"Background\"=\"49 54 58\""},
        {"\"ButtonAlternativeFace\"=\"200 0 0\""},
        {"\"ButtonDkShadow\"=\"154 154 154\""},
        {"\"ButtonFace\"=\"49 54 58\""},
        {"\"ButtonHilight\"=\"119 126 140\""},
        {"\"ButtonLight\"=\"60 64 72\""},
        {"\"ButtonShadow\"=\"60 64 72\""},
        {"\"ButtonText\"=\"219 220 222\""},
        {"\"GradientActiveTitle\"=\"49 54 58\""},
        {"\"GradientInactiveTitle\"=\"49 54 58\""},
        {"\"GrayText\"=\"155 155 155\""},
        {"\"Hilight\"=\"119 126 140\""},
        {"\"HilightText\"=\"255 255 255\""},
        {"\"InactiveBorder\"=\"49 54 58\""},
        {"\"InactiveTitle\"=\"49 54 58\""},
        {"\"InactiveTitleText\"=\"219 220 222\""},
        {"\"InfoText\"=\"159 167 180\""},
        {"\"InfoWindow\"=\"49 54 58\""},
        {"\"Menu\"=\"49 54 58\""},
        {"\"MenuBar\"=\"49 54 58\""},
        {"\"MenuHilight\"=\"119 126 140\""},
        {"\"MenuText\"=\"219 220 222\""},
        {"\"Scrollbar\"=\"73 78 88\""},
        {"\"TitleText\"=\"219 220 222\""},
        {"\"Window\"=\"35 38 41\""},
        {"\"WindowFrame\"=\"49 54 58\""},
        {"\"WindowText\"=\"219 220 222\""}
    }; 
    // Adding configuration to file
    printf("Setting dark...\n");
    // Accesing to file user.reg
    int max_len = sizeof(char) * 200;
    char *location_reg = malloc(max_len);
    check_memory_allocation(location_reg);
    snprintf(location_reg, max_len, "%s/PhotoshopCC/user.reg", location);
    // Write in the final of file (append mode)
    FILE *user_reg = fopen(location_reg, "a");
    check_memory_allocation(user_reg);
    for (int i = 0; i < 31; i++) 
    {
        fprintf(user_reg, "\n%s", colors[i]);
    }
    // Close file
    fclose(user_reg);
    // Free memory of location reg
    free(location_reg);
}

void change_desktop(char *location)
{
    // Open the file .desktop and change the Exec command for start Photoshop
    int max_len = sizeof(char) * 160;
    char *set_env = malloc(max_len);
    check_memory_allocation(set_env);
    snprintf(set_env, max_len, "Path=%s", location);
    // Exec command for open Photoshop.exe file
    max_len = sizeof(char) * 400;
    char *location_exe = malloc(max_len);
    check_memory_allocation(location_exe);
    snprintf(location_exe, max_len, "Exec=env WINEPREFIX=\"%1$s/PhotoshopCC\" wine64 %1$s/PhotoshopCC/Photoshop-CC/Photoshop.exe", location);
    // Copy .desktop file (if the user want another installation in other location this is useful)
    system("cp ./files/photoshop-cc.desktop ./files/photoshop.desktop");
    // Open file
    FILE *location_desktop = fopen("./files/photoshop.desktop", "a");
    check_memory_allocation(location_desktop);
    // Append the command in which the exec command is executed (set the env)
    fprintf(location_desktop, "%s\n", set_env);
    // Append the command for exec Photoshop
    fprintf(location_desktop, "%s\n", location_exe);
    // Close the file of desktop
    fclose(location_desktop);
    // Free memory
    free(set_env);
    free(location_exe);
}
